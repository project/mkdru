<?php
/**
 * @file
 * Facet block content.
 * 
 * $class: Class to mark the facet container. Facet name prefixed with mkdru-facet-.
 */
?>
<div class="mkdru-facet <?php print $class; ?>"> </div>
