<?php
/*
 * @file
 * Containers for search results and record detail.
 */
?>
<div class="mkdru-detail"></div>
<div class="mkdru-results">
  <div class="mkdru-above mkdru-above-below"><span class="mkdru-counts"></span><span class="mkdru-status"></span></div>
  <div class="mkdru-navi"></div>
  <ul class="mkdru-result-list"></ul>
  <div class="mkdru-below mkdru-above-below"><span class="mkdru-pager"></span></div>
</div>

